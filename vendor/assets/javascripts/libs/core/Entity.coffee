#= require_self
#= require_tree ./components
GSL.component={}
class GSL.Entity
  constructor: (options) ->
    @updates=[]
#    @raw=options
    util.raw @, options
    util.mixin @, options, false
    if options.require
      @require=options.require
    if @parent then @parent.e.push @
    @checkForRelationals()
    GSL.setupCallbacks.push(@)

  setup:()->

    @components = new GSL.Importer GSL.component,@
#    @components.add ["saveable"]
#    @initializeTable()
#    delete @required
    if @require
      @components.add @require
#      delete @require

    ArrayRemove(GSL.setupCallbacks, GSL.setupCallbacks.indexOf @)

  checkForRelationals:()->
    for key in Object.keys(@)
      if StringScan(key,/_ids$/g).length!=0
        newKey=key.replace(/_ids$/,"")+"s"
        @[newKey]=[]
        for val in @[key]
          val["EntityType"]=newKey
          val["find"]=()->

            self=@
            JSLINQ(window[GSL.REF][self.EntityType]).First( (c)->
              return c if c.id==self.id
            )
          @[newKey].push(val)



  store:()->
#    amplify
    game[@EntityType]
  get:(key)->
    ko.utils.unwrapObservable(@[key])


class GSL.Importer
  constructor: (@from,@dest,@mixin=true)->
  add: (imports) ->
    imports = [].concat(imports)
    for imp in imports when not @[imp]?
      imp=imp
      if @from[imp]?
        @[imp] = new @from[imp]
        if @mixin
          for key,val of @[imp] when (key isnt "onadd" and key isnt "onremove" and key isnt "run" and val?)
            @dest[key] = val
        @[imp].onadd(@dest)

      else log 2,"mixin #{imp} does not exist!"
  remove: (imports) ->
    imports = [].concat(imports)
    for imp in imports when @[imp]?
      if @mixin
        for key,val of @[imp] when @dest[key]? and @dest[key] is @[imp][key]
          delete @dest[key]
      if @[imp].onremove then @[imp].onremove.call(@dest)
      delete @[imp]



