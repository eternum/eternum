
#=require ./fixtures
class GLib.Settings
  constructor:(parent,settings)->
    @parent = parent
    @config=settings
    @setUpCallbacks=[]

  set_up:()->
    for callback in @setUpCallbacks
      callback.func(callback.options)

    @set_up_attributes()
    @set_up_areas()
    @set_up_actions()
    @set_up_turn()
    @set_up_actors()
    new GLib.Module.Player(GLib.Settings.config.actors[0])
    delete GLib.Game.store.Actor[GLib.Game.store.Player.id()]
    setTimeout ->
      console.log("loaded")
      GLib.Game.Loader.message('Loaded')
    ,1000
  set_up_actors:()->
    for a in @config.actors
      new GLib.Module.Actor a
  set_up_turn:()->
    GLib.Game.Turn=new GLib.Mechanics.Turn()
  set_up_actions:()->
    for action in @config.actions
      new GLib.Module.Action(action)
  set_up_areas:()->
    for area in @config.areas
      new GLib.Module.Area(area)
  set_up_attributes:()->
    for attribute in @config.attributes
      new GLib.Module.Attribute(attribute)

GLib.Game.start=()->
  GLib.Settings=new GLib.Settings(CONFIG)
  GLib.Settings.set_up()

GLib.Game.restart=()->
  GLib.Settings.set_up()
  return

CONFIG={
    attributes: [{id:1,name:"STR"},{id:2,name:"DEX"}],
    areas: [
      {
        id:1,
        name:"One"
        _connections: [
          {id:2}
        ]
      },{
        id:2,
        name:"Two"
        _connections: [
          {id:1}
        ]
      }
    ],
    actions:[{id:1,name:"Boom"},{id:2,name:"Punch"}],
    actors:window.ActorFIXTURES,
    player: {
      id:2,
      name:"Player One",
      _area:1,
      _attributes:
        [
          {id:1,value:10},
          {id:2,value:10}
        ]
    }
  };
