class GLib.Mechanics.Turn
  constructor:->
    @current=ko.observable(0)
    @timing=[0,0]
  startTurn:()->
    @timing[1]=Date.now()
    $.Notify({
      caption: "Turn "+ @current()
      content: new Date(@timing[1]-@timing[0]).getMilliseconds()+"ms",
      timeout: 10000
    });
  endTurn:()->
    @timing[0]=Date.now()
    @moveAllNonPlayers()
    @current(@current()+1)

    @startTurn()
    return

  moveAllNonPlayers:()->
    console.log("Moving Chars")
    for actor in GLib.Module.Actor.all()
      if typeof(actor) !='function'
        console.log("Moving"+actor.id())
        actor.ai_decision()
