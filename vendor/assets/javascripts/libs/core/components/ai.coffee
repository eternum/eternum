class GSL.component.ai
  constructor:()->

  onadd:(ref)->
    @parent=ref
    @parent.addSubComponent('mind',GSL.component.ai, @parent)

  addSubComponent:(imp,@from,@dest)->
    if @from[imp]?
      @[imp] = new @from[imp]
      @[imp].onadd(@dest)

    else log 2,"mixin #{imp} does not exist!"

  choose:()->
    #is area too different from me?
    if @shouldIMove()
      @moveTo @area()._connections().sample()

  shouldIMove:()->
    move=0
    move+= 1 - @area().crowded()
    console.debug move
    if move > 1 then return true
    move +=  @area().dangerousness(@) / 1000
    console.debug move
    if move > 1 then return true

class GSL.component.ai.mind
  onadd:(@parent)->
    @decisions=[]




class GSL.component.ai.mind.decision
  constructor:(@options)->
    util.mixin @, options
