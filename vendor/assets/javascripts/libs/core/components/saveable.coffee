

class GSL.component.saveable
  initializeTable:()->
    unless @tableExists()
      @createTable()
    unless @recordExists()
      @createRecord()

  createTable:()->
    console.log window[GSL.REF]
    amplify.store(@tableName,@)
#    amplify.store(@tableName,window[GSL.REF][@tableName])
  createRecord:()->
    table=JSON.parse(amplify.store(@tableName))
    amplify.store(@tableName)
#    @table().push(@toJSON())
#    amplify.store(@tableName,table)
#    catch
#      return false
  table:()->
    ret=amplify.store(@tableName)
    if typeof ret is 'undefined'
      return undefined
    else
      return ret

  tableIsEmpty:()->
    [].concat(@table()).length is 0
  tableExists:()->
    amplify.store(@tableName) != undefined
  recordExists:()->
    return false unless @table()
    return false unless @getRecord()

  toJSON:()->
    JSON.stringifyOnce(@raw)


  setRecord:()->
    return false unless @getRecord()
    table=@table()
    index=table.indexOf(JSON.stringify(@getRecord()))
    record=@toJSON()
    table[index]=record
    amplify.store(@tableName,table)
    return record

  getRecord:()->
    return undefined unless @table()
    return undefined unless @table().length > 0
    ret=JSLINQ(@table()).First (item)->
      item.id==@id
    if null isnt ret
      return JSON.parse(ret)
    else
      return null

  get:(col)->
    return false unless @getRecord()
    @getRecord()[col]

  set:(col,val)->
    return false unless @getRecord()
    @raw[col]=val




  onadd:(@parent)->
    @parent.tableName=@parent.raw.EntityType
    @initializeTable()
