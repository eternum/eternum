class GSL.component.Transform
  constructor:(options)->
    @position= new GSL.component.Vector(options.position)
    @rotation= new GSL.component.Vector(options.rotation)
    @scale= new GSL.component.Vector(options.scale)


class GSL.component.Vector
  constructor:(args)->
    @x=args.x
    @y=args.y
    @z=args.z