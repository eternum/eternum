
rgbToHex=(r, g, b) ->
  "#" + ((1 << 24) + (parseInt(r) << 16) + (parseInt(g) << 8) + parseInt(b)).toString(16).slice(1);


class GSL.component.area

  onadd:(ref)->
    @parent=ref
    @parent.area=@
    @parent.natural_capacity = 30
    @parent.highest_morality=10000
    @parent.danger_level = @highest_morality * 0.2
    @parent.danger_tolerance = 0.25
  doAction:(action,callback)->

  explore:(level=0)->
    console.log("Explore Successful.")
  expand:(level=0)->
    console.log("Expand Successful")
  exterminate:(level=0)->
    console.log("Exterminate Not Allowed.")
  exploit:(level=0)->
    console.log("Exploit Successful.")

  crowded:->
    @natural_capacity / @actorsIn().length
  danger_abs: ->
    actors=@actorsIn()
    mor=actors.map((z) ->
      z.morality()
    ).reduce ((a, b) ->
      a + b
    ), 0
    mor / actors.length
  dangerColor:()->
    inver=255/@highest_morality
    c=@danger_abs()
    return rgbToHex((@highest_morality - c)*inver,c*inver,0)
  dangerousness:(actor)->
    Math.abs( @danger_abs() - actor.get('morality'))


  connections:()->
    JSLINQ(window[GSL.REF][@EntityType]).Where((c)->
      return @["_connections"].indexOf(c.id)!=-1
    ).items

  actorsIn:()->
    if typeof(window[GSL.REF]) isnt 'undefined'
      window[GSL.REF].find_by('actors','_area',@id)
class GSL.component.inArea

  onadd:(ref)->
    @parent=ref
    @parent.area=->
      JSLINQ(window[GSL.REF]['areas']).First (item) ->
        return item.get('id') is 1
    @parent.moveTo=(id)->
      @_area(id)