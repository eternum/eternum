class GSL.Action
  constructor:(options)->
    util.mixin @, options

  has_points:(entity)->
    entity.attributes()[@cost_type] > @cost
