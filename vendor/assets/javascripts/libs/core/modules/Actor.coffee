class GLib.Module.Actor extends GLib.Object
  @klass: @
  @klass_name: @name
  @store: ->
    GLib.Game.store[@klass_name]
  constructor:(params)->
    if typeof(params)is 'undefined'
      params={}
    if typeof(params.area) is 'undefined'
      params.area=Object.random(GLib.Game.store.Area).get("id")
    @player=false
    super(params)
    @last_moved = -1;
    @Morality = new Actor.Morality @morality()
    @area GLib.Module.Area.find(params.area)
  moveTo:(id)->
    a=GLib.Module.Area.find(id)
    if !@player
      if @decide_to_move(a)
        @area a
        @last_moved=GLib.Game.Turn.current()
    else

      @area a
      @last_moved=GLib.Game.Turn.current()
  ai_decision:()->
    e=Math.random()*100
    if e < 10
      @moveTo(@area().connections().sample().id())

  decide_to_move:(a)->
    crowded=a.crowdedness() > @area().crowdedness()
    just_moved= @last_moved+2 < GLib.Game.Turn.current()
    return crowded && just_moved



class GLib.Module.Actor.Attribute extends GLib.Module.Attribute
  constructor:(params)->
    @attribute_id=params.id
    @attribute=GLib.Module.Attribute.find(@attribute_id)
    @value = params.value
    @max=@value
  change_by:(val)->
    v=@value + parseFloat(val)
    @value=v
    if (@max < @value)
      @max=@value

class GLib.Module.Actor.Action extends GLib.Module.Action
  constructor:(args)->
    super(args)
class GLib.Module.Player extends GLib.Module.Actor
  constructor:(params)->
    super(params)
    @player=true
    GLib.Game.store.Player=@

class GLib.Module.Actor.Morality extends GLib.Object
  @level:(int)->
    ret = switch
      when int < 1000
         "super villain"
      when int < 2000
        "villain"
      when int < 4000
        "anti-hero"
      when int < 5000
        "dark hero"
      when int < 6000
        "hero"
      when int < 8000
        "superhero"
      when int < 10000
        "paragon"

  constructor:(params)->
    @to_i = params
    @to_s = ko.computed(->
      GLib.Module.Actor.Morality.level(@to_i)
    ,@)
    super(params)