natural_capacity = 10
highest_morality=10000
danger_level = highest_morality * 0.2
danger_tolerance = 0.1
class GSL.Area
  constructor:(options)->
    util.mixin @, options
###
  connected_to:()->
    retval=[]
    for area in @connections
      retval.push(Area.find(area))
    retval
  actors_in:() ->
    if typeof(GLib.Module.Actor) isnt 'undefined'
      GLib.Module.Actor.find_by('area',@)
  crowdedness: ->
    natural_capacity / @actors_in().length
  dangerousness: ->
    actors=GLib.Game.store.Player.area().actors_in()
    mor=actors.map((z) ->
      z.morality()
      ).reduce ((a, b) ->
        a + b
      ), 0
    ( mor / actors.length)/100
class GLib.Module.Area.Connection extends GLib.Object
  constructor:(params)->
    @id=ko.observable(params.id)
  to:()->
    GLib.Module.Area.find @id()

###