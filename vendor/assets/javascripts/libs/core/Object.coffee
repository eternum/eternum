class GLib.Object extends GLib.Module
  @include instanceProperties
  @klass= @
  @klass_name= @klass.name
  @store: ->
    GLib.Mechanics.store[@klass_name]
  constructor: (args)->
    self=@
    for obj of args
      if obj.match(/^_/) != null
        b=obj.replace("_","").toString()
        self[b]=ko.observableArray([])
        for attr in args[obj]
          c=[b.charAt(0).toUpperCase(),b.slice(1).singularize()].join("")
          m=eval(["GLib","Module",@constructor.name,c].join('.'))
          at=new m(attr)
          self[b].push(at)
      else
        if(typeof this[obj] == "undefined")  then this[obj]=ko.observable(args[obj]) else this[obj]
    if args.id
      this.save()
  get:(key)->
    fn=@[key]
    fn()
  set: (key, value)->
    @[key](value)

  save:->
    if typeof @id isnt "undefined"
      if typeof GLib.Mechanics.store[@constructor.name] is "undefined"
        GLib.Mechanics.store[@constructor.name]={}
      GLib.Mechanics.store[@constructor.name][@id()]=@
  store:GLib.Mechanics.store[@constructor.name]
  @find:(id)->
    @klass.find_by('id',id)[0]
  @find_by: (attr,value)->
    ret=[]
    if typeof @store() is 'undefined' then return []
    for c of @store()
      if ko.utils.unwrapObservable(@store()[c][attr]) is value then ret.push(@store()[c])
    return ret
  @all: ->
    ret=[]
    if typeof @store() is 'undefined' then return []
    for c of @store()
      ret.push(@store()[c])
    return ret