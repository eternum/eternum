#= require_self
#=require ./Object
#=require ./mechanics/Turn
#=require ./mechanics/Loading
#=require ./modules/Attribute
#=require ./modules/Action
#=require ./modules/Actor
#=require ./modules/Area


ko.trackChange = (observable, key) ->
  store = amplify.store.localStorage

  #initialize from stored value, or if no value is stored yet,
  #use the current value
  value = store(key) or observable()

  #track the changes
  observable.subscribe (newValue) ->
    store key, newValue or null
    observable newValue  unless ko.toJSON(observable()) is ko.toJSON(newValue)

  observable value #restore current value

ko.persistChanges = (vm, prefix) ->
  prefix = ""  if prefix is `undefined`
  for n of vm
    observable = vm[n]
    key = prefix + n
    if ko.isObservable(observable) and not ko.isComputed(observable)

      #track change of observable
      ko.trackChange observable, key

      #force load
      observable()

ko.isComputed = (instance) ->
  return false  if (instance is null) or (instance is `undefined`) or (instance.__ko_proto__ is `undefined`)
  return true  if instance.__ko_proto__ is ko.dependentObservable
  ko.isComputed instance.__ko_proto__ # Walk the prototype chain

window.instanceProperties =
  save: ->
    self=@
    old=amplify.store(self.constructor.klass_name)
    if typeof old is "undefined" then old=[]
    try
      id=self.id()
    catch
      id=self.id
    old[id]=JSON.parse(ko.toJSON(self))
    amplify.store(self.constructor.klass_name, old)
  build: (args)->
    self=@

moduleKeywords = ['extended', 'included']

class GLib.Module
  @klass_name=''
  @extend: (obj) ->
    for key, value of obj when key not in moduleKeywords
      @[key] = value
    obj.extended?.apply(@)
    this
  @include: (obj) ->
    for key, value of obj when key not in moduleKeywords
      @::[key] = value
    obj.included?.apply(@)
    this
