#= require ../vendor/knockout
#= require ../vendor/amplify.min

# require ../vendor/three.min.js
#= require ../vendor/JSLINQ.js
#= require ../vendor/promise
#= require ../vendor/amplify.min

#= require_self

#= require ./core/Entity
#= require ./core/ApiManager
#= require ./core/modules/Action
#= require ./core/modules/Attribute
#= require ./core/modules/Area

ko.extenders.localStore = (target, key) ->
  value = amplify.store(key) or target()
  result = ko.computed(
    read: target
    write: (newValue) ->
      amplify.store key, newValue
      target newValue
      return
  )
  result value
  result


window.log = (level, args...) ->
  switch level
    when 1 then func = console.error or console.log
    when 2 then func = console.warn or console.log
    when 3 then func = console.info or console.log
    when 4 then func = console.debug or console.log
  func.call(console,"(GSL)", args...)
JSON.stringifyOnce = (obj, replacer, space) ->
  cache = []
  json = JSON.stringify(obj, (key, value) ->
    if typeof value is "object" and value isnt null

      # circular reference found, discard key
      return  if cache.indexOf(value) isnt -1

      # store value in our collection
      cache.push value
    (if replacer then replacer(key, value) else value)
  , space)
  cache = null
  json
window.util =
# Makes a new canvas
  canvas: -> document.createElement("canvas")

  imgToCanvas: (i) ->
    c = @canvas()
    c.src = i.src; c.width = i.width; c.height = i.height
    cx = c.getContext "2d"
    cx.drawImage i,0,0,i.width,i.height
    c

  isArray: (value) ->
    Object::toString.call(value) is '[object Array]'

  remove: (a,val) ->
    idx = a.indexOf(val)
    idx and a.splice idx, 1
  raw: (obj,mixin)->

    for name,method of mixin when method isnt null

#      if method.slice
#        obj[name] = (method.slice(0))
#      else if parseInt(method)
#        obj[name]= (method)
#      else
        obj[name] = method


  mixin: (obj, mixin) ->
    for name, method of mixin when method isnt null
      if name is "id" || name is "EntityType" || method is undefined
        obj[name]=method
      else if method.slice
        obj[name] = method.slice(0)
      else if parseInt(method)
        obj[name]=method
      else
        obj[name] = method
    obj

  IE: ->
    `//@cc_on navigator.appVersion`
window.ArrayRemove = (obj, from, to) ->

  rest = obj.slice((to or from) + 1 or obj.length)
  obj.length = (if from < 0 then obj.length + from else from)
  obj.push.apply obj, rest
window.StringScan = (obj,re) ->
  throw "ducks"  unless re.global
  s = obj
  m = undefined
  r = []
  while m = re.exec(s)
    m.shift()
    r.push m
  r
class GLib
  constructor:(@options={}) ->


class Game
  constructor: (@options={}) ->
    GSL.REF=@options.REF
    @loaded=ko.observable(false)
    @currentActivity=ko.observable("Setting Up")
    @showBlocker= ko.computed =>
      @currentActivity() isnt null

  finalizeComponents:()->
    self=@
    until GSL.setupCallbacks.length==0
      GSL.setupCallbacks[0].setup()
      if GSL.setupCallbacks.length==0
        setTimeout ->
          self.loaded(true)
          GSL.Finalized()
        ,1000
  store:(klass)->
    @[klass.toLowerCase()]

  find:(klass,id)->
    JSLINQ(amplify.store(klass)).Find((item)->
      item.id==id
    )
#    @find_by(klass, 'id',id)[0]
  find_by: (klass,attr,value)->
    ret=[]
    for c of @store(klass)
      if ko.utils.unwrapObservable(@store(klass)[c][attr]) is value then ret.push(@store(klass)[c])
    return ret
  all:(klass) ->
    ret=[]
    if typeof @store(klass) is 'undefined' then return []
    for c of @store(klass)
      ret.push(@store(klass)[c])
    return ret

  start: (state) ->

    while GSL.setupCallbacks.length !=0
      console.log "STOPPING"
      @finalizeComponents()

    @switchState(state)
    @currentActivity(null)

  switchState: (state) ->
    @e = []
    @loop = new GameTurn @,@showFPS
    @loop and @loop.stop()
    @oldState = @state
    @state = state
    @state.setup.call(@state,@)

    @loop.add [test]
    if @turnCalls
      @loop.add [func] for func in @turnCalls
    @loop.start()

test=()->
  console.log "Boom"

class GameTurn
  constructor: (@parent,@showFPS) ->
    @averageFPS= new RollingAverage 20
    @call=[]
    @nextTurn=ko.observable(false)
    @turn=ko.observable(0)
  start:->
    @advance()
  advance: ->
    @paused = @stopped = false
    @nextTurn true
    GSL.ticker.call window, @loop

  loop: =>
    if @nextTurn
      @turn(@turn()+1)
      @nextTurn(false)
      unless @stopped or @paused
        func.call(@parent.state, @parent) for func in @call
  add: (func) ->
    @call = @call.concat func
  stop: ->


class GameLoop
  constructor: (@parent,@showFPS) ->
    @fps = 0
    @averageFPS = new RollingAverage 20
    @call = []

  # Starts the loop, running each function in @call every tick
  start: ->
    @paused = @stopped = false
    firstTick = currentTick = lastTick = (new Date()).getTime()
    GSL.ticker.call window, @loop
  # loop function that is called every tick
  loop: =>
    @currentTick = (new Date()).getTime()
    @dt = (@currentTick - @lastTick) or 17
    @fps = @averageFPS.add(1000/@dt)
    unless @stopped or @paused
      if @dt > 20 then @dt = 17
#        func.call(@parent.state,@parent,@dt/1000) for func in @call
    unless @stopped
      GSL.ticker.call window, @loop
    if @showFPS then @parent.context.fillText("fps:#{@fps} step:#{@dt}",10,10)
    @lastTick = @currentTick
  # Pauses the game loop, loop still runs, but no functions are called
  pause: ->
    @paused = !@paused
  # Stops the game loop
  stop: ->
    @stopped = true

  # adds a function or an array of functions to the loop
  add: (func) ->
    @call = @call.concat func

class RollingAverage
  constructor: (@size) ->
    @values = new Array @size
    @count = 0

  add: (value) ->
    @values = @values[1...@size]
    @values.push value
    if @count < @size then @count++
    return ((@values.reduce (t, s) -> t+s)/@count) | 0



###
#= require ./core/Settings
class GLib.Mechanics
  constructor:(t)->
    @parent=t
    @Loader =new GLib.Mechanics.Loading()
    @parent.Settings.setUpCallbacks.push(@Loader.message("Setting Up The Game"))
  @store = {}

class GLib.Mechanics.Loading
  constructor:()->
    @text=ko.observable('Loading Components')
    @loaded=ko.observable(false)
  message:(m)->
    if(m)=="Loaded"
      @loaded(true)
      m=null
    @text(m)
    m

###
GSL= {REF:''}

GSL.GameBinding = (id)->
  rgame=window[GSL.REF]
  rgame.loop.nextTurn.subscribe( (val)->
    if (val) is true then rgame.loop.advance()
  )
#  ko.applyBindings()





GSL.setupCallbacks=[]
GSL.ticker = window.requestAnimationFrame or
  window.webkitRequestAnimationFrame or
  window.mozRequestAnimationFrame or
  window.oRequestAnimationFrame or
  window.msRequestAnimationFrame or
  (tick) -> window.setTimeout(tick, 1000/60)


GSL.ready = (f) ->
  document.addEventListener "DOMContentLoaded", ->
    document.removeEventListener "DOMContentLoaded", arguments.callee, false
    f()
GSL.Finalized = (f)->
  console.info("Sending Finalized")
  f()

GSL.Game=Game
GSL.GameLoop = GameLoop
GSL.Views={}
#GSL.Entity = Entity

@GSL=GSL